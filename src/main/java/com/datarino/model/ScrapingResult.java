package com.datarino.model;

import java.util.List;

public class ScrapingResult {

    private List<BashMessage> bashMessages;

    private CrawlingStats crawlingStats;

    //region getters and setters
    public List<BashMessage> getBashMessages() {
        return bashMessages;
    }

    public void setBashMessages(List<BashMessage> bashMessages) {
        this.bashMessages = bashMessages;
    }

    public CrawlingStats getCrawlingStats() {
        return crawlingStats;
    }

    public void setCrawlingStats(CrawlingStats crawlingStats) {
        this.crawlingStats = crawlingStats;
    }
    //endregion

}

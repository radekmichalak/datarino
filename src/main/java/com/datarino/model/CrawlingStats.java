package com.datarino.model;

import java.util.Date;

public class CrawlingStats {

    public CrawlingStats(Date startDate,Date endDate,int numberOfPages, int numberOfBashMessages)
    {
        this.numOfBashMessages = numberOfBashMessages;
        this.totalProcessingTimeInMs = (int)((endDate.getTime() - startDate.getTime()));
        this.avgNumOfMsForPageRetrieving = totalProcessingTimeInMs != 0 ? totalProcessingTimeInMs / numberOfPages : 0;
        this.avgNumOfMsForBashMessageRetrieving = totalProcessingTimeInMs != 0 ? totalProcessingTimeInMs / numberOfBashMessages: 0;
    }

    private int numOfBashMessages;

    private int avgNumOfMsForPageRetrieving;

    private int avgNumOfMsForBashMessageRetrieving;

    private int totalProcessingTimeInMs;

    //region getters and setters
    public int getNumOfBashMessages() {
        return numOfBashMessages;
    }

    public int getAvgNumOfMsForPageRetrieving() {
        return avgNumOfMsForPageRetrieving;
    }

    public int getAvgNumOfMsForBashMessageRetrieving() {
        return avgNumOfMsForBashMessageRetrieving;
    }

    public int getTotalProcessingTimeInMs() {
        return totalProcessingTimeInMs;
    }
    //endregion
}

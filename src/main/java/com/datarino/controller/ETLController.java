package com.datarino.controller;

import com.datarino.model.BashMessage;
import com.datarino.model.CrawlingStats;
import com.datarino.model.ScrapingResult;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.jsoup.Jsoup;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class ETLController {

    private final String        BASE_URL = "http://bash.org.pl/latest";
    private final Logger logger             = LoggerFactory.getLogger(this.getClass());


    @GetMapping("/bashorg/{numberOfPageToProcess}")
    public ScrapingResult  getBashMessages(@PathVariable int numberOfPageToProcess) {

        ScrapingResult      scrapingResult        = new ScrapingResult();
        List<BashMessage>   bashMessageList       = new ArrayList<>();
        UriBuilder          uriBuilder            = UriComponentsBuilder.fromUriString(BASE_URL);
        CrawlingStats       crawlingStats;
        Document            document              = null;

        Date startDate = new Date();

        for(int currentPage = 1;currentPage <= numberOfPageToProcess;currentPage++)
        {
            uriBuilder.replaceQueryParam("page",String.valueOf(currentPage));
            String currentPageURL = uriBuilder.build().toString();

            try {document = Jsoup.connect(currentPageURL).get();}
            catch(Exception handlerException) {logger.error("jsoup.connection.error");}

            Elements postElements   = document.getElementsByAttributeValueContaining("class","q post");

            postElements.forEach( postElement ->
            {
               bashMessageList.add(extractBashMessage(postElement));
            });
        }

        Date endDate = new Date();

        crawlingStats = new CrawlingStats(startDate,endDate,numberOfPageToProcess,bashMessageList.size());

        scrapingResult.setBashMessages(bashMessageList);
        scrapingResult.setCrawlingStats(crawlingStats);

        return scrapingResult;
    }

    protected BashMessage extractBashMessage(Element postElement) {

        //Properties are identified by following class/id name in HTML :
        //content -> class "quote post-content post-body"
        //points  -> class " points"
        //id      -> attribute "id"

       BashMessage bashMessage = new BashMessage();

       if (null != postElement) {

           String strId             = postElement.attr("id") ;
           Elements pointsElements  = postElement.getElementsByAttributeValueContaining("class", " points");
           Elements contentElements = postElement.getElementsByAttributeValueContaining("class", "quote post-content post-body");

           Element pointsElement  = pointsElements  != null ? pointsElements.first() : null;
           Element contentElement = contentElements != null ? contentElements.first() : null;

           //substring(1) to delete # element from id value
           long id        = Long.valueOf   (        strId != null ? strId.substring(1)    : "0");
           int points     = Integer.valueOf(pointsElement != null ? pointsElement.text()  : "0");
           String content =                contentElement != null ? contentElement.text() : "";

           bashMessage.setId(id);
           bashMessage.setContent(content);
           bashMessage.setPoints(points);
       }
       return bashMessage;
   }
}

package com.datarino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class DatarinoApplication extends SpringBootServletInitializer {


    public static void main(String[] args) {
        SpringApplication.run(DatarinoApplication.class, args);
    }

    //I am using WAR to host app in cloud foundry
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(DatarinoApplication.class);
    }
}
